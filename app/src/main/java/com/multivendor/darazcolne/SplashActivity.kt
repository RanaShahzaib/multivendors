package com.multivendor.darazcolne

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.multivendor.darazcolne.Helper.PreferencesHelper

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        android.os.Handler().postDelayed(object : Runnable {
            override fun run() {
                if(PreferencesHelper(applicationContext).getLogin()){
                    var intent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(intent)
                }else{
                    var intent = Intent(applicationContext, LoginActivity::class.java)
                    startActivity(intent)
                }
                finish()
            }
        }, 4000)
    }
}
