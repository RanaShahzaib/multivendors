package com.multivendor.darazcolne

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.gitonway.lee.niftynotification.lib.Configuration
import com.gitonway.lee.niftynotification.lib.Effects
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_product.*
import org.json.JSONObject

class ProductActivity : AppCompatActivity() {

    var arr = ArrayList<String>()
    lateinit var helper: PreferencesHelper;
    val TAG = this.javaClass.simpleName;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)
        pageIndicatorView.count = 4
        pageIndicatorView.selection = 0
        helper = PreferencesHelper(this);

        var details = intent.getStringExtra("details")
        var json = JSONObject(details)
        name.text = json.getString("name")
        detail.text = json.getString("detail")
        price.text = "RS. "+json.getString("price")

        arr.add(json.getString("image"))
        arr.add(json.getString("image1"))
        arr.add(json.getString("image2"))
        arr.add(json.getString("image3"))

        buy_now.setOnClickListener{
            run {
                var progress = SpotsDialog(this@ProductActivity, R.style.LoadingDialogTheme)
                progress.setCancelable(false)
                progress.show()

                var client = AsyncHttpClient();
                var params = RequestParams();
                params.put("product_id", json.getString("id"))
                params.put("qty", "1")
                client.addHeader("Authorization", "Bearer "+helper.getToken())
                client.addHeader("Accept", "application/json")
                client.post(Api.ADD_TO_CART, params, object: AsyncHttpResponseHandler(){
                override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                        Toast.makeText(this@ProductActivity, "Product Added to cart", Toast.LENGTH_SHORT).show()
                    Log.v(TAG, "Response : "+ String(responseBody));
                    progress.dismiss()
                }

                override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                    Log.v(TAG, "Error : "+ error?.localizedMessage)
                    progress.dismiss()
                }
            })
        }
        }

        setTitle("Product")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewpager.adapter = ViewPagerAdapter(arr, this)
        viewpager.startAutoScroll(4000)
        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                pageIndicatorView.selection = position
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }

    class ViewPagerAdapter:PagerAdapter{

        var arr : ArrayList<String>;
        var context : Context;

        constructor(arr : ArrayList<String>, context: Context){
            this.arr = arr
            this.context = context
        }

        override fun getCount(): Int {
            return arr.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return `object` == view as ImageView;
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            var inflater = LayoutInflater.from(context)
            var view = inflater.inflate(R.layout.product_detail_slider_img, container, false) as ImageView;
            //view.setImageResource(arr[position])
            Glide.with(view).load(arr[position]).into(view)
            container?.addView(view)
            return view
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container?.removeView(`object` as ImageView)
        }
    }

    fun showNifty(){
        var cfg = Configuration.Builder()
                .setAnimDuration(700)
                .setDispalyDuration(1500)
                .setBackgroundColor("#f78a3c")
                .setTextColor("#FFFFFFFF")
                .setIconBackgroundColor("#f78a3c")
                .setTextPadding(5)                      //dp
                .setViewHeight(48)                      //dp
                .setTextLines(2)                        //You had better use setViewHeight and setTextLines together
                .setTextGravity(Gravity.CENTER)         //only text def  Gravity.CENTER,contain icon Gravity.CENTER_VERTICAL
                .build();
        NiftyNotificationView.build(this,"Product has been added to cart successfully", Effects.thumbSlider,R.id.mLyout, cfg)
                .setIcon(R.drawable.ic_shopping_cart)         //You must call this method if you use ThumbSlider effect
                .show()
    }
}
