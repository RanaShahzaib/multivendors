package com.multivendor.darazcolne.Helper

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by Rana Shahzaib on 1/8/2018.
 */
class PreferencesHelper(context: Context){

    private val KEY_LOGIN = "login"
    private val KEY_NAME = "name"
    private val KEY_EMAIL = "email"
    private val KEY_PHONE = "phone"
    private var KEY_TOKEN = "token"
    private var KEY_ADDRESS = "address"
    private var KEY_ID = "id"

    var context:Context;
    init {
        this.context = context;
    }

    fun setLogin(key:Boolean){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(KEY_LOGIN, key).apply()
    }
    fun setName(key:String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_NAME, key).apply()
    }
    fun setEmail(key:String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_EMAIL, key).apply()
    }
    fun setPhone(key:String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_PHONE, key).apply()
    }

    fun getLogin():Boolean{
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(KEY_LOGIN, false)
    }
    fun getName():String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_NAME, "")
    }
    fun getEmail():String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_EMAIL, "")
    }
    fun getPhone():String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_PHONE, "")
    }

    fun setToken(token:String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_TOKEN, token).apply()
    }

    fun getToken():String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_TOKEN, "")
    }

    fun setId(id:String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_ID, id).apply()
    }
    fun getId():String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_ID, "0")
    }
    fun setAddress(address:String){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_ADDRESS, address).apply()
    }
    fun getAddress():String{
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_ADDRESS, "");
    }
}