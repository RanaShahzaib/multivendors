package com.multivendor.darazcolne.Helper

/**
 * Created by Rana Shahzaib on 1/8/2018.
 */
class Api{
    companion object {
        val BASE_URL = "http://vendor.itsecpro.net/api/"
        val LOGIN_URL = BASE_URL + "login"
        val REGISTER_URL = BASE_URL + "register"
        val GET_DETAILS = BASE_URL + "get-details"
        val VENDOR_PRODUCTS = BASE_URL + "getVendorProducts"
        val SEARCH_CATEGORY = BASE_URL + "searchCategory"
        val FILTERED_SEARCH = BASE_URL + "filteredSearch"
        val ADD_TO_CART = BASE_URL + "addToCart"
        val UPDATE_CART = BASE_URL + "updateCart"
        val DELETE_CART = BASE_URL + "deleteCart"
        val DELETE_FROM_CART = BASE_URL + "deleteFromCart"
        val CART = BASE_URL+"getCart";
    }
}