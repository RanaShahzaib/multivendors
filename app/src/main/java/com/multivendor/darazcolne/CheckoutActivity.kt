package com.multivendor.darazcolne

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper

import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_checkout.*
import org.json.JSONObject


class CheckoutActivity : AppCompatActivity() {
    lateinit var helper :PreferencesHelper;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        helper = PreferencesHelper(this)
        setTitle("Checkout")
        setContentView(R.layout.activity_checkout)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        checkout.setOnClickListener{
            var card = card_input_widget.card;
        if(card==null){
            Toast.makeText(this, "Card info is not valid ", Toast.LENGTH_SHORT).show()
        }else{
            proceed()
        }
        }
    }

    private fun proceed() {
        var progress = SpotsDialog(this, R.style.LoadingDialogTheme)
        progress.setTitle("Loading...")
        progress.setCancelable(false)
        progress.show()

        android.os.Handler().postDelayed(object :Runnable{
            override fun run() {
                progress.dismiss()
                deleteCart()
            }
        }, 2000);
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }

    fun deleteCart(){
        var progress = SpotsDialog(this, R.style.LoadingDialogTheme)
        progress.setTitle("Loading...")
        progress.setCancelable(false)
        progress.show()

        var client = AsyncHttpClient();
        client.addHeader("Authorization", "Bearer "+helper.getToken())
        client.addHeader("Accept", "application/json")
        var params = RequestParams();
        client.get(Api.DELETE_CART, object: AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                finish()
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                progress.dismiss()
            }
        })
    }
}
