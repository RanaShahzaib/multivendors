package com.multivendor.darazcolne

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setTitle("Login")
        login.setOnClickListener { view ->
            login()
        }
        link_signup.setOnClickListener { view ->
            var intent = Intent(applicationContext, RegisterActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
    }

    fun login(){
        if(email.text.toString().length==0){
            email.error = "Email cannot be empty"
            return
        }
        if(password.text.toString().length==0){
            email.error = "Password cannot be empty"
            return
        }
        var client = AsyncHttpClient();
        var params = HashMap<String, String>()
        params.put("email", email.text.toString())
        params.put("password", password.text.toString())
        var progress = SpotsDialog(this, R.style.LoadingDialogTheme)
        progress.setCancelable(false)
        progress.show()
        client.post(Api.LOGIN_URL, RequestParams(params), object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                var jsonResponse = JSONObject(String(responseBody))
                var token = jsonResponse.getJSONObject("success").getString("token")
                var helper = PreferencesHelper(applicationContext);
                helper.setLogin(true)
                helper.setToken(token)
                startActivity(Intent(applicationContext, MainActivity::class.java))
                finish()
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                progress.dismiss()
                Toast.makeText(applicationContext, "Network error!", Toast.LENGTH_SHORT).show()
            }

        })
    }
}
