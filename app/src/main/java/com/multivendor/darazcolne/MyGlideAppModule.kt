package com.multivendor.darazcolne

/**
 * Created by Rana Shahzaib on 1/29/2018.
 */
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MyGlideAppModule : AppGlideModule()