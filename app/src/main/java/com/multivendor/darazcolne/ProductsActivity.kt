package com.multivendor.darazcolne

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuItem
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Adapters.ProductsAdapter
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper
import com.multivendor.darazcolne.Models.Product
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_products.*
import org.json.JSONObject

class ProductsActivity : AppCompatActivity() {
    lateinit var productsAdapter:ProductsAdapter;
    lateinit var helper:PreferencesHelper
    var productsList = ArrayList<Product>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        helper = PreferencesHelper(this)
        setContentView(R.layout.activity_products)
        setTitle(intent.getStringExtra("title"))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        productsAdapter = ProductsAdapter(productsList)
        recyclerView.adapter = productsAdapter
        if(intent.hasExtra("response")){
            addProducts(intent.getStringExtra("response"))
        }

        if(intent.hasExtra("category")){
            doSearch(intent.getStringExtra("category"))
        }
    }

    private fun addProducts(response:String) {
        try{
            var json = JSONObject(response)
            var items = json.getJSONArray("success")
            for(i in 0..items.length()-1){
                var item = items.getJSONObject(i)
                productsList.add(Product(item.getString("id"), item.getString("name"), item.getString("image"), item.getString("price"), item.getString("category"), item))
            }
            productsAdapter.notifyDataSetChanged()
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }

    private fun doSearch(query:String) {
        var progress = SpotsDialog(this, R.style.LoadingDialogTheme)
        progress.setTitle("Loading...")
        progress.setCancelable(false)
        progress.show()

        var client = AsyncHttpClient();
        client.addHeader("Authorization", "Bearer "+helper.getToken())
        client.addHeader("Accept", "application/json")
        var params = RequestParams();
        params.put("key", query)
        client.post(Api.FILTERED_SEARCH, params, object: AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                var intent = Intent(this@ProductsActivity, ProductsActivity::class.java);
                intent.putExtra("response", String(responseBody))
                intent.putExtra("title", query)
                startActivity(intent)
                finish()
                Log.v("Search", "Response : "+ String(responseBody));
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                Log.v("Search", "Error : "+ error?.localizedMessage)
                progress.dismiss()
            }
        })
    }
}
