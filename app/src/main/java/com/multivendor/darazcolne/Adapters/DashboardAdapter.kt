package com.multivendor.darazcolne.Adapters

import android.content.Intent
import android.media.Image
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.multivendor.darazcolne.Models.Featured
import com.multivendor.darazcolne.Models.RV
import com.multivendor.darazcolne.Models.Shop
import com.multivendor.darazcolne.ProductsActivity
import com.multivendor.darazcolne.R

/**
 * Created by Rana Shahzaib on 1/7/2018.
 */
class DashboardAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>{

    companion object {
        public var TYPE_SHOPS = 1
        public var TYPE_RV = 2
        public var TYPE_FEATURED = 3

    }

    var type:Int;
    var list : ArrayList<Object>

    constructor(type:Int, list:ArrayList<Object>):super(){
        this.type = type
        this.list = list;
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        when(type){
            TYPE_SHOPS->{
                var item = list.get(position) as Shop
                var holderN = holder as ShopeVH
                holderN.image.setImageResource(item.image)
            }
            TYPE_FEATURED->{
                var item = list.get(position) as Featured
                var holderN = holder as FeaturedVH
                holderN.image.setImageResource(item.image)
                holderN.title.text = item.title
                holderN.body.text = item.body
                holder.itemView.setOnClickListener { view ->
                    holder.itemView.context.startActivity(Intent(holder.itemView.context, ProductsActivity::class.java));
                }
            }
            TYPE_RV->{
                var item = list.get(position) as RV
                var holderN = holder as VH
                holderN.image.setImageResource(item.image)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        when(type){
            TYPE_SHOPS->{
                return ShopeVH(LayoutInflater.from(parent?.context).inflate(R.layout.item_shop, parent, false))
            }
            TYPE_FEATURED->{
                return FeaturedVH(LayoutInflater.from(parent?.context).inflate(R.layout.item_featured, parent, false))
            }
            TYPE_RV->{
                return VH(LayoutInflater.from(parent?.context).inflate(R.layout.item_rv, parent, false))
            }
        }
        return ShopeVH(LayoutInflater.from(parent?.context).inflate(R.layout.item_shop, parent, false))
    }


    class ShopeVH(itemView:View) : RecyclerView.ViewHolder(itemView){
        var image : ImageView
        init {
            image = itemView.findViewById(R.id.img)
        }
    }

    class FeaturedVH(itemView:View) : RecyclerView.ViewHolder(itemView){
        var image : ImageView
        var title : TextView
        var body : TextView
        init {
            image = itemView.findViewById(R.id.img)
            title = itemView.findViewById(R.id.title)
            body = itemView.findViewById(R.id.body)
        }
    }

    class VH(itemView:View) : RecyclerView.ViewHolder(itemView){
        var image : ImageView
        init {
            image = itemView.findViewById(R.id.img)
        }
    }

}