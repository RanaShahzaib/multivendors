package com.multivendor.darazcolne.Adapters

import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Fragments.CartFragment
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper
import com.multivendor.darazcolne.Models.Cart
import com.multivendor.darazcolne.R
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog

/**
 * Created by Rana Shahzaib on 1/28/2018.
 */
class CartRecyclerViewAdapter:RecyclerView.Adapter<CartRecyclerViewAdapter.CartViewHolder>{
    var list = ArrayList<Cart>()
    var callback :CartFragment.Callback
    constructor(list:ArrayList<Cart> , callback: CartFragment.Callback){
        this.list = list;
        this.callback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CartRecyclerViewAdapter.CartViewHolder {
        return CartViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.cart_item, parent, false))
    }


    override fun onBindViewHolder(holder: CartRecyclerViewAdapter.CartViewHolder, position: Int) {
        var item = list.get(position)
        holder.title.text = item.productTitle
        holder.brand.text = item.companyName
        holder.quantity.setSelection(Integer.valueOf(item.size)-1)
        holder.price.text = item.price
        holder.delete.setOnClickListener {
            val builder: AlertDialog.Builder
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = AlertDialog.Builder(holder.itemView.context, android.R.style.Theme_Material_Dialog_Alert)
            } else {
                builder = AlertDialog.Builder(holder.itemView.context)
            }
            builder.setTitle("Remove item?")
                    .setMessage("Are you sure you want to remove this item?")
                    .setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                        delete(item.productId, holder.itemView.context)
                    })
                    .setNegativeButton(android.R.string.no, DialogInterface.OnClickListener { dialog, which ->
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class CartViewHolder(view : View):RecyclerView.ViewHolder(view){
        var title : TextView
        var price : TextView
        var brand : TextView
        var size : TextView
        var quantity : Spinner
        var img : ImageView
        var delete:ImageView
        var sizeContainer : LinearLayout
        init {
            title = view.findViewById(R.id.title)
            price = view.findViewById(R.id.price)
            brand = view.findViewById(R.id.shopName)
            quantity = view.findViewById(R.id.quantity)
            img = view.findViewById(R.id.img);
            size = view.findViewById(R.id.size)
            sizeContainer = view.findViewById(R.id.size_container)
            delete = view.findViewById(R.id.delete)
        }
    }

    fun delete(id:String, context: Context){
        var progress = SpotsDialog(context, R.style.LoadingDialogTheme)
        progress.setTitle("Loading...")
        progress.setCancelable(false)
        progress.show()

        var client = AsyncHttpClient();
        client.addHeader("Authorization", "Bearer "+ PreferencesHelper(context).getToken())
        client.addHeader("Accept", "application/json")
        var params = RequestParams();
        params.put("product_id", id)
        client.post(Api.CART,params ,object: AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                Toast.makeText(context, "Item has been deleted", Toast.LENGTH_SHORT).show()
                var json = String(responseBody)
                callback.onClick()
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                progress.dismiss()
                Toast.makeText(context, "Error! Try again a minute later", Toast.LENGTH_SHORT).show()
            }
        })
    }
}