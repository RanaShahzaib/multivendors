package com.multivendor.darazcolne.Adapters

import android.content.Intent
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


import com.multivendor.darazcolne.Fragments.dummy.DummyContent.DummyItem
import com.multivendor.darazcolne.ProductsActivity
import com.multivendor.darazcolne.R

class NavAdapter: RecyclerView.Adapter<NavAdapter.ViewHolder>{
    var mDarwer:DrawerLayout;

    constructor(mDrawerLayout: DrawerLayout){
        this.mDarwer = mDrawerLayout;
    }
    var arr  = arrayOf ("SMARTPHONES",
    "MOBILE & TABLET ACCESSORIES",
    "FEATURE PHONES",
    "TABLETS",
    "UAE CLOTHING",
    "WESTERN CLOTHING",
    "WINTER CLOTHING",
    "LINGERIE & SLEEPWEAR",
    "WATCHES",
    "WOMEN'S SHOES",
    "MEN'S SHOES",
    "KIDS SHOES",
    "PREMIUM CLOTHING",
    "MAKE UP",
    "SKIN CARE",
    "HAIR CARE",
    "HEALTH CARE",
    "PERSONAL CARE",
    "FRAGRANCES",
    "MENS GROOMING",
    "HAIR COLOR",
    "LARGE APPLIANCES",
    "SMALL APPLIANCES",
    "LAPTOPS",
    "COMPONENTS & SPARE PARTS",
    "STORAGE",
    "PRINTERS & SCANNERS",
    "PERIPHERALS & ACCESSORIES",
    "GAMING PC & PERIPHERALS",
    "CONSOLES",
    "VIDEO GAMES",
    "GAMING ACCESSORIES",
    "LCD",
    "AUDIO & VIDEO",
    "CAMERAS",
    "Camera Accessories",
    "KITCHEN & DINING",
    "HOME DɃOR",
    "BEDDING",
    "FURNITURE",
    "HOUSEHOLD SUPPLIES",
    "HOME STORAGE SUPPLIES",
    "HOME IMPROVEMENTS",
    "LIGHTING",
    "LAWN & GARDEN",
    "EXERCISE & FITNESS",
    "STRENGTH TRAINING EQUIPMENT",
    "SPORTS & FITNESS GADGETS",
    "TEAM SPORTS",
    "SPORT SHOES & CLOTHING",
    "FITNESS ACCESSORIES",
    "RACKET SPORTS",
    "SPORTS BAGS & ACCESSORIES",
    "TRAVEL ACCESSORIES",
    "KIDS' FASHION",
    "BOYS' FASHION",
    "GIRLS' FASHION",
    "BABIES' FASHION",
    "Arts & Crafts for Kids",
    "TEA, COFFEE & BEVERAGES",
    "LAUNDRY & HOME CARE",
    "COOKING ESSENTIALS",
    "CANNED & PACKAGED FOODS",
    "SNACKS",
    "HOME IMPROVEMENTS",
    "BREAKFAST",
    "CHOCOLATES & DESSERTS",
    "PET SUPPLIES",
    "BOOKS & STATIONERY",
    "AUTOMOTIVE & MOTORCYCLES",
    "LIFESTYLE ACCESSORIES",
    "VOUCHERS & SERVICES");


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_nav, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = arr[position]
        holder.mContentView.text = arr[position]

        holder.mView.setOnClickListener {
            mDarwer.closeDrawer(Gravity.START)
            var intent = Intent(holder.mView.context, ProductsActivity::class.java)
            intent.putExtra("title", holder.mItem)
            intent.putExtra("category", holder.mItem)
            holder.mView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return arr.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mContentView: TextView
        var mItem: String? = null

        init {
            mContentView = mView.findViewById(R.id.content) as TextView
        }

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
