package com.multivendor.darazcolne.Adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.multivendor.darazcolne.Models.Product
import com.multivendor.darazcolne.MyGlideAppModule
import com.multivendor.darazcolne.ProductActivity
import com.multivendor.darazcolne.R

/**
 * Created by Rana Shahzaib on 1/7/2018.
 */
class ProductsAdapter:RecyclerView.Adapter<RecyclerView.ViewHolder> {
    var list : ArrayList<Product>

    constructor(list:ArrayList<Product>):super(){
        this.list = list
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        var item = list[position]
        var vh = holder as VH
        vh.price.text = "RS. "+item.price
        vh.title.text = item.name
        vh.company.text = item.brand
        Glide.with(vh.itemView)
                .load(item.img).into(vh.image)
        vh.itemView.setOnClickListener { view ->
            var intent = Intent(vh.itemView.context, ProductActivity::class.java)
            intent.putExtra("details", item.product.toString())
            vh.itemView.context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return VH(LayoutInflater.from(parent?.context).inflate( R.layout.item_product, parent , false))
    }

    class VH(view : View) : RecyclerView.ViewHolder(view){
        var image : ImageView
        var title : TextView
        var price : TextView
        var company : TextView
        init {
            image = view.findViewById(R.id.img)
            title = view.findViewById(R.id.title)
            price = view.findViewById(R.id.price)
            company = view.findViewById(R.id.company)
        }
    }
}