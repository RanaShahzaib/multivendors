package com.multivendor.darazcolne

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.multivendor.darazcolne.Helper.PreferencesHelper
import com.multivendor.darazcolne.Models.Featured
import com.multivendor.darazcolne.Models.RV
import com.multivendor.darazcolne.Models.Shop
import kotlinx.android.synthetic.main.activity.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.view.LayoutInflater
import android.widget.TextView
import android.support.v4.view.ViewPager
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.multivendor.darazcolne.Adapters.NavAdapter
import com.multivendor.darazcolne.Adapters.ViewPagerAdapter
import com.multivendor.darazcolne.Fragments.*
import com.multivendor.darazcolne.Helper.Api
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import org.json.JSONObject


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var shopsList = ArrayList <Shop>()
    var rvList = ArrayList <RV>()
    var featuredList = ArrayList <Featured>()
    var storiesHeight = 0

    val TAG = this.javaClass.simpleName;

    lateinit var helper:PreferencesHelper;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity)
        setSupportActionBar(toolbar)
        setTitle("Daraz")
        helper = PreferencesHelper(this);
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        setupViewPager(viewPager);
        left_drawer.layoutManager = LinearLayoutManager(this)
        left_drawer.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        left_drawer.adapter = NavAdapter(drawer_layout)
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons()
        loadProfile()
    }

    private fun loadProfile() {
        var progress = SpotsDialog(this, R.style.LoadingDialogTheme)
        progress.setTitle("Loading...")
        progress.setCancelable(false)
        progress.show()

        var client = AsyncHttpClient();
        client.addHeader("Authorization", "Bearer "+helper.getToken())
        client.addHeader("Accept", "application/json")
        client.post(Api.GET_DETAILS, object:AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                val json = JSONObject(String(responseBody))
                val user = json.getJSONObject("success")
                val id = user.getString("id")
                val name = user.getString("name")
                val email = user.getString("email")
                val address = user.getString("address")
                val phone = user.getString("contact")
                helper.setId(id)
                helper.setName(name)
                helper.setEmail(email)
                helper.setAddress(address)
                helper.setPhone(phone)

                Log.v(TAG, "Response : "+ String(responseBody));
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                Log.v(TAG, "Error : "+ error?.localizedMessage)
                progress.dismiss()
            }
        })
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(DashboardFragment(), "Home")
        adapter.addFrag(FavouritesFragment(), "Saved")
        adapter.addFrag(CartFragment(), "Cart")
        viewPager.adapter = adapter
    }

    private fun setupTabIcons() {

        val tabOne = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView
        tabOne.text = "Home"
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home, 0, 0)
        tabLayout.getTabAt(0)?.setCustomView(tabOne)

        val tabTwo = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView
        tabTwo.text = "Saved"
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_favorites, 0, 0)
        tabLayout.getTabAt(1)?.setCustomView(tabTwo)

        val tabThree = LayoutInflater.from(this).inflate(R.layout.custom_tab, null) as TextView
        tabThree.text = "Cart"
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_shopping_cart, 0, 0)
        tabLayout.getTabAt(2)?.setCustomView(tabThree)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings ->{
                logout();
                return true
            }
            R.id.search->{
                startActivity(Intent(this, SearchActivity::class.java))
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    fun logout(){
        PreferencesHelper(this).setLogin(false)
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
