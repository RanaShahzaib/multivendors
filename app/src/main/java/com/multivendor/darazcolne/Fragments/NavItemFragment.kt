package com.multivendor.darazcolne.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.multivendor.darazcolne.Adapters.NavAdapter

import com.multivendor.darazcolne.R
import com.multivendor.darazcolne.Fragments.dummy.DummyContent
import com.multivendor.darazcolne.Fragments.dummy.DummyContent.DummyItem

/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [OnListFragmentInteractionListener]
 * interface.
 */
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class NavItemFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_navitem, container, false)

        // Set the adapter
        var view = v.findViewById<RecyclerView>(R.id.list)
        view.layoutManager = LinearLayoutManager(context)
        view.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        //view.adapter = NavAdapter()
        return view
    }


}
