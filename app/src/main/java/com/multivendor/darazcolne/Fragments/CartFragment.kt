package com.multivendor.darazcolne.Fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.telecom.Call
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Adapters.CartRecyclerViewAdapter
import com.multivendor.darazcolne.CheckoutActivity
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper
import com.multivendor.darazcolne.Models.Cart

import com.multivendor.darazcolne.R
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import org.json.JSONObject
import org.w3c.dom.Text


/**
 * A simple [Fragment] subclass.
 */
class CartFragment : Fragment(){
    lateinit var helper:PreferencesHelper
    var totalPrice = 0;
    var list = ArrayList<Cart>();
    lateinit var recyclerView:RecyclerView;
    lateinit var adapter:CartRecyclerViewAdapter;
    lateinit var totalTxt:TextView;
    lateinit var priceTxt:TextView;
    lateinit var discount:TextView;
    lateinit var proceed:Button
    lateinit var empty:View;


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_cart, container, false)
        recyclerView = view.findViewById(R.id.cartRecyclerView);
        adapter = CartRecyclerViewAdapter(list, object : Callback{
            override fun onClick() {
                addItems()
            }
        });
        recyclerView.adapter = adapter;
        helper = PreferencesHelper(context!!)
        totalTxt = view.findViewById(R.id.total)
        priceTxt = view.findViewById(R.id.price)
        discount = view.findViewById(R.id.discount);
        proceed = view.findViewById(R.id.proceedToCheckOut)
        proceed.setOnClickListener{
            startActivity(Intent(context, CheckoutActivity::class.java))
        }
        empty = view.findViewById(R.id.empty)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        return view;
    }

    override fun onResume() {
        super.onResume()
        addItems()
    }

    fun addItems(){
        var progress = SpotsDialog(context, R.style.LoadingDialogTheme)
        progress.setTitle("Loading...")
        progress.setCancelable(false)
        progress.show()

        var client = AsyncHttpClient();
        client.addHeader("Authorization", "Bearer "+helper.getToken())
        client.addHeader("Accept", "application/json")
        var params = RequestParams();
        list.clear()
        client.get(Api.CART, object: AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                var json = JSONObject(String(responseBody))
                var arr = json.getJSONArray("success");
                for(i in 0..arr.length()-1){
                    var obj = arr.getJSONObject(i)
                    var prodId = obj.getString("product_id")
                    var cartId = obj.getString("id")
                    var qty = obj.getString("qty")
                    var price = obj.getString("price")
                    totalPrice = totalPrice+Integer.valueOf(price)*Integer.valueOf(qty)
                    list.add(Cart(cartId, prodId, "Product Title", "", price, qty, "Company name", ""))
                }
                Log.d("Cart","Size : "+ list.size)
                Log.d("Response ","Response : "+json.toString())
                adapter.notifyDataSetChanged()
                if(list.size==0){
                    empty.visibility = View.VISIBLE;
                }else if(list.size>0){
                    empty.visibility = View.GONE;
                }
                updateUi();
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                progress.dismiss()
            }
        })
        adapter.notifyDataSetChanged()
    }

    private fun updateUi() {
        totalTxt.setText(totalPrice.toString())
        priceTxt.setText(totalPrice.toString())
        discount.setText("0")
    }

    public interface Callback{
        fun onClick()
    }

}// Required empty public constructor
