package com.multivendor.darazcolne.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import com.multivendor.darazcolne.Adapters.DashboardAdapter
import com.multivendor.darazcolne.Models.Featured
import com.multivendor.darazcolne.Models.RV
import com.multivendor.darazcolne.Models.Shop

import com.multivendor.darazcolne.R



/**
 * A simple [Fragment] subclass.
 */
class DashboardFragment : Fragment() {

    var shopsList = ArrayList <Shop>()
    var rvList = ArrayList <RV>()
    var featuredList = ArrayList <Featured>()
    var storiesHeight = 0
    lateinit var rv1:RecyclerView;
    lateinit var shops:RecyclerView;
    lateinit var featured_stories:RecyclerView;


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater!!.inflate(R.layout.fragment_dashboard, container, false)
        addShops();
        addRv();
        addFeatured();
        rv1 = view.findViewById(R.id.rv1);
        shops = view.findViewById(R.id.shops)
        featured_stories = view.findViewById(R.id.featured_stories)
        rv1.adapter = DashboardAdapter(DashboardAdapter.TYPE_RV, rvList as ArrayList<Object>)
        rv1.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        shops.adapter = DashboardAdapter(DashboardAdapter.TYPE_SHOPS, shopsList as ArrayList<Object>)
        shops.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        featured_stories.adapter = DashboardAdapter(DashboardAdapter.TYPE_FEATURED, featuredList as ArrayList<Object>)
        featured_stories.layoutManager = GridLayoutManager(context, 2)
        featured_stories.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, storiesHeight)
        return view
    }

    private fun addFeatured() {
        featuredList.clear()
        featuredList.add( Featured(R.drawable.b4, "Bla", "It's on 60% discount"))
        featuredList.add( Featured(R.drawable.b4, "Bla", "It's on 60% discount"))
        featuredList.add( Featured(R.drawable.b4, "Bla", "It's on 60% discount"))
        featuredList.add( Featured(R.drawable.b4, "Bla", "It's on 60% discount"))
        featuredList.add( Featured(R.drawable.b4, "Bla", "It's on 60% discount"))
        featuredList.add( Featured(R.drawable.b4, "Bla", "It's on 60% discount"))
        storiesHeight = featuredList.size*140
        Toast.makeText(context, "Size : "+storiesHeight, Toast.LENGTH_SHORT).show()
    }

    private fun addRv() {
        rvList.clear()
        rvList.add(RV(R.drawable.b2))
        rvList.add(RV(R.drawable.b4))
        rvList.add(RV(R.drawable.b5))
        rvList.add(RV(R.drawable.b6))
    }

    private fun addShops() {
        shopsList.clear()
        shopsList.add(Shop(R.drawable.shop1))
        shopsList.add(Shop(R.drawable.shop2))
        shopsList.add(Shop(R.drawable.shop3))
        shopsList.add(Shop(R.drawable.shop4))
        shopsList.add(Shop(R.drawable.shop5))
        shopsList.add(Shop(R.drawable.shop6))
        shopsList.add(Shop(R.drawable.shop7))
        shopsList.add(Shop(R.drawable.shop8))
    }

}// Required empty public constructor
