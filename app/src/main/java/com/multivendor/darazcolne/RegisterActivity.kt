package com.multivendor.darazcolne

import android.app.ProgressDialog
import android.content.AsyncQueryHandler
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper
import cz.msebera.android.httpclient.Header
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setTitle("Register")
        signup.setOnClickListener{
            view ->
            register()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }

    fun register(){
        if(first_name.text.length==0){
            first_name.error="First name cannot be empty"
        }
        if(last_name.text.length==0){
            last_name.error = "Last name cannot be empty"
        }
        if(email.text.length==0){
            email.error = "Email cannot be emnpty"
            return
        }
        if(password.text.length==0){
            password.error = "Password cannot be empty"
        }

        if(phone.text.length==0){
            phone.error = "Phone cannot be empty"
        }

        var params = HashMap<String, String>()
        params.put("name", first_name.text.toString()+" "+last_name.text.toString())
        params.put("email", email.text.toString())
        params.put("password", password.text.toString())
        params.put("c_password", password.text.toString())
        var progress = ProgressDialog(this, android.R.style.Theme_Material_Dialog)
        progress.setMessage("Please wait")
        progress.setCancelable(false)
        progress.show()
        var client = AsyncHttpClient()
        client.post(Api.REGISTER_URL, RequestParams(params), object : AsyncHttpResponseHandler() {
            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                progress.dismiss()
                Toast.makeText(applicationContext, "Network error!", Toast.LENGTH_SHORT).show()
            }

            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                var jsonResponse = JSONObject(String(responseBody))
                var token = jsonResponse.getJSONObject("success").getString("token")
                var helper = PreferencesHelper(applicationContext);
                helper.setLogin(true)
                helper.setName(params.get("name")!!)
                helper.setEmail(params.get("email")!!)
//                helper.setPhone(params.get("phone")!!)
                helper.setToken(token)
                startActivity(Intent(applicationContext, MainActivity::class.java))
                finish()
            }
        })
    }
}

