package com.multivendor.darazcolne

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.RequestParams
import com.multivendor.darazcolne.Helper.Api
import com.multivendor.darazcolne.Helper.PreferencesHelper
import cz.msebera.android.httpclient.Header
import dmax.dialog.SpotsDialog
import kotlinx.android.synthetic.main.activity_search.*
import org.json.JSONObject

class SearchActivity : AppCompatActivity() {
    val TAG = this.javaClass.simpleName;
    lateinit var helper: PreferencesHelper;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        helper = PreferencesHelper(this);
        searchView.setOnEditorActionListener(object :TextView.OnEditorActionListener{
            override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
                if(p1 == EditorInfo.IME_ACTION_SEARCH){
                    doSearch(searchView.text.toString())
                }
                return true
            }
        })
        arrow.setOnClickListener{
            finish()
        }
    }

    private fun doSearch(query:String) {
        var progress = SpotsDialog(this, R.style.LoadingDialogTheme)
        progress.setTitle("Loading...")
        progress.setCancelable(false)
        progress.show()

        var client = AsyncHttpClient();
        client.addHeader("Authorization", "Bearer "+helper.getToken())
        client.addHeader("Accept", "application/json")
        var params = RequestParams();
        params.put("key", query)
        client.post(Api.FILTERED_SEARCH, params, object: AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray) {
                progress.dismiss()
                var intent = Intent(this@SearchActivity, ProductsActivity::class.java);
                intent.putExtra("response", String(responseBody))
                intent.putExtra("title", query)
                startActivity(intent)
                finish()
                Log.v(TAG, "Response : "+ String(responseBody));
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?, error: Throwable?) {
                Log.v(TAG, "Error : "+ error?.localizedMessage)
                progress.dismiss()
            }
        })
    }
}
