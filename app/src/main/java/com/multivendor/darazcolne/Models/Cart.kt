package com.multivendor.darazcolne.Models

data class Cart(var id : String,
                var productId:String,
                var productTitle : String,
                var imageUrl : String,
                var price: String,
                var size: String,
                var companyName:String,
                var color:String)