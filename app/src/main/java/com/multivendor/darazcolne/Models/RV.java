package com.multivendor.darazcolne.Models;

/**
 * Created by Rana Shahzaib on 1/7/2018.
 */

public class RV {
    int image;

    public RV(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
