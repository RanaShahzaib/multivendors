package com.multivendor.darazcolne.Models;

/**
 * Created by Rana Shahzaib on 1/7/2018.
 */

public class Featured {
    int image;
    String title;
    String body;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Featured(int image, String title, String body) {
        this.image = image;
        this.body = body;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
