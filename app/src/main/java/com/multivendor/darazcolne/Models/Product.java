package com.multivendor.darazcolne.Models;

import org.json.JSONObject;

/**
 * Created by Rana Shahzaib on 1/7/2018.
 */

public class Product {
    String name;
    String img;
    String price;
    String brand;
    String id;
    JSONObject product;

    public Product(String id, String name, String img, String price, String brand, JSONObject product) {
        this.name = name;
        this.img = img;
        this.price = price;
        this.brand = brand;
        this.id = id;
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JSONObject getProduct() {
        return product;
    }

    public void setProduct(JSONObject product) {
        this.product = product;
    }
}
