package com.multivendor.darazcolne.Models;

/**
 * Created by Rana Shahzaib on 1/7/2018.
 */

public class Shop {
    int image;

    public Shop(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
